import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.util.Date.from

plugins {
    kotlin("jvm") version "1.4.32"
    application
}

group = "me.nokhwezikholane"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}
val mainClassPath = "MainKt"
dependencies {
    implementation("junit:junit:3.8.2")
    implementation("junit:junit:4.13.1")
    implementation("junit:junit:4.13.1")
    implementation("junit:junit:4.13.1")
    implementation("junit:junit:4.13.1")
    testImplementation(kotlin("test-junit"))
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set(mainClassPath)
    @Suppress("DEPRECATION")
    mainClassName = mainClassPath
}



