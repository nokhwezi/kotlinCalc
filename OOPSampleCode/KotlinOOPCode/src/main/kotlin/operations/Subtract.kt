package operations

import calculations.Operation

class Subtract : Operation {
    override fun calculateResult(left: Double, right: Double): Double {
        return left - right
    }
}
