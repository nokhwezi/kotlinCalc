package operations

import calculations.Operation

class Addition : Operation {
    override fun calculateResult(left: Double, right: Double): Double {
        return left + right
    }
}