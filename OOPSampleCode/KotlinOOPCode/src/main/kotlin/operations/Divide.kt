package operations

import calculations.Operation

class Divide : Operation {
    override fun calculateResult(left: Double, right: Double): Double {
        if (right == 0.0) {
            throw ArithmeticException("\noperations.Divide by zero")
        }
        return left / right
    }
}
