package operations

import calculations.Operation

class Multiply : Operation {
    override fun calculateResult(left: Double, right: Double): Double {
        if (right == 0.0) {
            throw ArithmeticException("\nMultiplication by zero")
        }
        return left * right
    }
}
