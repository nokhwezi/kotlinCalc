package calculations

interface Operation {
    fun calculateResult(left: Double, right: Double): Double
}