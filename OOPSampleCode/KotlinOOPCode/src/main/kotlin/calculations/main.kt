package calculations

import calculations.Calculator
import java.util.*


object Main {
    private val scanner = Scanner(System.`in`)
    @JvmStatic fun main(args:Array<String>) {
        println("Write two numbers and +, -, * or / sign")
        var operator:Char = 0.toChar()
        var firstNum = 0.0
        var secondNum = 0.0
        try
        {
            firstNum = scanner.nextDouble()
            secondNum = scanner.nextDouble()
            operator = scanner.next().single()
        }
        catch (e:InputMismatchException) {
           println("Invalid operation found")
        }
        val calculator = Calculator()
        print(calculator.makeCalculation(firstNum, secondNum, operator))
    }
}