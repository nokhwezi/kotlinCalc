package calculations

import operations.Addition
import operations.Divide
import operations.Multiply
import operations.Subtract
import java.util.HashMap

class Calculator internal constructor() {
    private val operationMap: MutableMap<Char, Operation> = HashMap()
    fun makeCalculation(operand1: Double, operand2: Double, operation: Char): Double {
        val operationMapValue = operationMap[operation]
        return operationMapValue!!.calculateResult(operand1, operand2)
    }

    init {
        operationMap['+'] = Addition()
        operationMap['-'] = Subtract()
        operationMap['*'] = Multiply()
        operationMap['/'] = Divide()
    }
}