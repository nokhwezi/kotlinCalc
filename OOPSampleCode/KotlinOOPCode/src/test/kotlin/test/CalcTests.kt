package test

import operations.Addition
import operations.Divide
import operations.Multiply
import operations.Subtract
import junit.framework.Assert.assertEquals

import org.junit.Test

class CalcTests {
    private var firstNumber = 10.0
    private var secondNumber = 0.0
    private var result = 0.0

    @Test
    fun testAddition() {
        val calculator = Addition()
        result = calculator.calculateResult(firstNumber, secondNumber)
       assertEquals(firstNumber, result)
    }

    @Test
    fun testSubtract() {
        val calculator = Subtract()
        result = calculator.calculateResult(firstNumber, secondNumber)
        assertEquals(firstNumber, result)
    }

    @Test(expected = ArithmeticException::class)
    fun testMultiply() {
        val calculator = Multiply()
        result = calculator.calculateResult(firstNumber, secondNumber)
        assertEquals(firstNumber, result)
    }

    @Test(expected = ArithmeticException::class)
    fun testDivide() {
        val calculator = Divide()
        result = calculator.calculateResult(firstNumber, secondNumber)
        assertEquals(firstNumber, result)
    }
}
