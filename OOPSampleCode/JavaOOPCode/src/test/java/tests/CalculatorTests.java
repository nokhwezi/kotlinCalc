package tests;

import com.company.Operations.Addition;
import com.company.Operations.Division;
import com.company.Operations.Multiply;
import com.company.Operations.Subtract;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CalculatorTests {
    private double firstNumber;
    private double secondNumber;
    private double result;

    @Before
    public void setUp() {
        firstNumber = 10;
        secondNumber = 0;
    }

    @Test
    public void testAddition() {
        Addition calculator = new Addition();
        result = calculator.calculateResult(firstNumber, secondNumber);
        assertEquals(firstNumber,result);
    }

    @Test
    public void testSubtract() {
        Subtract calculator = new Subtract();
        result = calculator.calculateResult(firstNumber, secondNumber);
        assertEquals(firstNumber,result);
    }

    @Test(expected = ArithmeticException.class)
    public void testMultiply() {
        Multiply calculator = new Multiply();
        result = calculator.calculateResult(firstNumber, secondNumber);
        assertEquals(firstNumber,result);
    }

    @Test(expected = ArithmeticException.class)
    public void testDivide() {
        Division calculator = new Division();
        result = calculator.calculateResult(firstNumber, secondNumber);
        assertEquals(firstNumber,result);
    }

}
