package com.company.Operations;

import com.company.Calculation.Operation;

public class Subtract implements Operation {
    @Override
    public double calculateResult(double left, double right) {
        return left - right;
    }
}
