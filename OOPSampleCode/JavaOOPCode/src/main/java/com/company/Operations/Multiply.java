package com.company.Operations;

import com.company.Calculation.Operation;

public class Multiply implements Operation {
    @Override
    public double calculateResult(double left, double right) {
        if (right == 0) {
            throw new ArithmeticException("\nMultiplication by zero");
        }
        return left * right;
    }
}
