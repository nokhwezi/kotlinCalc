package com.company.Operations;

import com.company.Calculation.Operation;

public class Division implements Operation {
    @Override
    public double calculateResult(double left, double right) {
        if (right == 0) {
            throw new ArithmeticException("\nDivision by zero");
        }
        return left / right;
    }
}
