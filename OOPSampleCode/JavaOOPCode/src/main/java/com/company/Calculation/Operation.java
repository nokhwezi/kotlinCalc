package com.company.Calculation;

public interface Operation {
    double calculateResult(double left, double right);
}
