package com.company.Calculation;

import com.company.Operations.Addition;
import com.company.Operations.Division;
import com.company.Operations.Multiply;
import com.company.Operations.Subtract;

import java.util.HashMap;
import java.util.Map;

public class Calculator {
    private Map<Character, Operation> operationMap = new HashMap<>();

    public Calculator() {
        operationMap.put('+', new Addition());
        operationMap.put('-', new Subtract());
        operationMap.put('*', new Multiply());
        operationMap.put('/', new Division());
    }

    double makeCalculation(double operand1, double operand2, char operation) {
        Operation operationMapValue = operationMap.get(operation);
        return operationMapValue.calculateResult(operand1, operand2);

    }
}
