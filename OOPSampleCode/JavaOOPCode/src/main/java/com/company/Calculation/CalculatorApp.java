package com.company.Calculation;

import com.company.Calculation.Calculator;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CalculatorApp {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Write two numbers and +, -, * or / sign");
        char operator = 0;
        double firstNum = 0;
        double secondNum = 0;
        try {
            firstNum = scanner.nextDouble();
            secondNum = scanner.nextDouble();
            operator = scanner.next().charAt(0);
        } catch (InputMismatchException e) {
            System.out.println("invalid operation entered");
        }
        Calculator calculator = new Calculator();
        System.out.print(calculator.makeCalculation(firstNum, secondNum, operator));

    }
}
